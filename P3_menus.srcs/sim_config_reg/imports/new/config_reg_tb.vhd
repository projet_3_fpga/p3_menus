----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 06/22/2021 06:19:28 PM
-- Design Name:
-- Module Name: config_reg_tb - stimulus
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity config_reg_tb is
end config_reg_tb;

architecture stimulus of config_reg_tb is
  constant CLK_PERIOD : time := 10ns;

  signal reg_select: std_logic_vector (2 downto 0);
  signal reg_wr : std_logic;
  signal reg: std_logic_vector (9 downto 0);
  signal reg_int : integer;

  signal reg_hue : std_logic_vector (9 downto 0);
  signal reg_sat : std_logic_vector (9 downto 0);
  signal reg_val : std_logic_vector (9 downto 0);

  signal rst : std_logic;
  signal clk : std_logic;

begin
  DUT: entity work.config_reg(rtl)
    port map (
      reg_select => reg_select,
      reg_wr => reg_wr,
      reg => reg,

      reg_hue => reg_hue,
      reg_sat => reg_sat,
      reg_val => reg_val,

      rst => rst,
      clk => clk
    );

  drive_clk : process is
  begin
    clk <= '0';
    wait for CLK_PERIOD / 2;
    clk <= '1';
    wait for CLK_PERIOD / 2;
  end process drive_clk;

  rst <= '1', '0' after 2*CLK_PERIOD;
  reg <= std_logic_vector(to_signed(reg_int, reg'length));
  
  test_bench: process is
  begin
    reg_int <= 0;
    wait for 3*CLK_PERIOD;

    -- N'écrire dans aucun registre
    reg_int <= 254;
    reg_select <= (others => '0');
    reg_wr <= '1', '0' after CLK_PERIOD;
    wait for 2*CLK_PERIOD;
    
    -- Écrire dans HUE
    reg_int <= -390;
    reg_select <= "100";
    reg_wr <= '1', '0' after CLK_PERIOD;
    wait for 2*CLK_PERIOD;
    
    -- Écrire dans SAT
    reg_int <= -255;
    reg_select <= "010";
    reg_wr <= '1', '0' after CLK_PERIOD;
    wait for 2*CLK_PERIOD;

    -- Écrire dans VAL
    reg_int <= 254;
    reg_select <= "001";
    reg_wr <= '1', '0' after CLK_PERIOD;    
    wait;

  end process test_bench;

end stimulus;
