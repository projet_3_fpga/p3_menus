----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/16/2021 10:27:36 PM
-- Design Name: 
-- Module Name: signed_to_str_tb - stimulus
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity signed_to_str_tb is
end signed_to_str_tb;

architecture stimulus of signed_to_str_tb is
  constant CLK_PERIOD : time := 10ns;
  
  signal signed_num : std_logic_vector(9 downto 0);
  signal string_addr : std_logic_vector(9 downto 0);
  signal clk : std_logic;
  signal en : std_logic;
begin

  DUT: entity work.signed_to_str(rtl)
    port map (
       signed_num => signed_num,
       string_addr => string_addr,
       clk  => clk,
       en   => en
    );
  
  drive_clk : process is
  begin
    clk <= '0';
    wait for CLK_PERIOD / 2;
    clk <= '1';
    wait for CLK_PERIOD / 2;
  end process drive_clk;

  test_bench: process is
  variable i : integer := 0;
  begin
    signed_num <= (others => '0');
    wait for 3*CLK_PERIOD;
    for i in -390 to 390 loop
      signed_num <= std_logic_vector(to_signed(i, signed_num'length));
      wait for CLK_PERIOD;
    end loop;
  end process test_bench;

  en <= '0', '1' after 2*CLK_PERIOD;
  
end stimulus;
