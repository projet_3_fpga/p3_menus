----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 06/18/2021 04:24:22 PM
-- Design Name:
-- Module Name: menu_msa_tb - stimulus
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity menu_msa_tb is
end menu_msa_tb;

architecture stimulus of menu_msa_tb is
  constant CLK_PERIOD : time := 10ns;

  -- Entrées
  signal buttons : std_logic_vector(3 downto 0);
  signal reg_hue : std_logic_vector(9 downto 0);
  signal reg_sat : std_logic_vector(9 downto 0);
  signal reg_val : std_logic_vector(9 downto 0);
  signal timer_dn : std_logic;

  -- Sorties
  signal disp : std_logic_vector(9 downto 0);
  signal reg_select : std_logic_vector(2 downto 0);
  signal reg_wr : std_logic;
  signal reg : std_logic_vector(9 downto 0);

  signal blink_en : std_logic;
  signal timer_en : std_logic;

  -- Misc.
  signal rst : std_logic;
  signal clk : std_logic;

  -- Constantes relatives aux boutons
  alias BTNL is buttons(0);
  alias BTND is buttons(1);
  alias BTNR is buttons(2);
  alias BTNU is buttons(3);
  constant BTNS_ALL_RELEASED : std_logic_vector(3 downto 0) := "0000";

begin

  DUT: entity work.menu_msa(rtl)
    port map (
      buttons => buttons,
      reg_hue => reg_hue,
      reg_sat => reg_sat,
      reg_val => reg_val,
      timer_dn => timer_dn,

      disp => disp,
      reg_select => reg_select,
      reg_wr => reg_wr,
      reg => reg,

      blink_en => blink_en,
      timer_en => timer_en,

      rst => rst,
      clk => clk
    );

  drive_clk : process is
  begin
    clk <= '0';
    wait for CLK_PERIOD / 2;
    clk <= '1';
    wait for CLK_PERIOD / 2;
  end process drive_clk;

  rst <= '1', '0' after 2*CLK_PERIOD;

  test_bench: process is
  begin
    -- State: start
    buttons <= BTNS_ALL_RELEASED;
    reg_hue <= (others => '0');
    reg_sat <= (others => '0');
    reg_val <= (others => '0');
    timer_dn <= '0';
    wait for 4*CLK_PERIOD;

    -- State: select_hue
    BTND <= '1';
    wait for CLK_PERIOD;

    -- State: wait_release_select_sat
    wait for 3*CLK_PERIOD;
    BTND <= '0';
    wait for 2*CLK_PERIOD;

    -- State: select_sat
    BTND <= '1';
    wait for CLK_PERIOD;

    -- State: wait_release_select_val
    wait for 3*CLK_PERIOD;
    BTND <= '0';
    wait for 2*CLK_PERIOD;

    -- State: select_val
    BTNU <= '1';
    wait for CLK_PERIOD;

    -- State: wait_release_select_sat
    wait for 3*CLK_PERIOD;
    BTNU <= '0';
    wait for 2*CLK_PERIOD;

    -- State: select_sat
    BTNR <= '1';
    wait for CLK_PERIOD;

    -- State: wait_release_set_reg
    wait for 3*CLK_PERIOD;
    BTNR <= '0';
    wait for 2*CLK_PERIOD;

    -- State: set_reg
    BTND <= '1';
    wait for CLK_PERIOD;

    --State: decr_sat
    wait for CLK_PERIOD;

    --State: wait_release_set_reg
    wait for 3*CLK_PERIOD;
    BTND <= '0';
    wait for 2*CLK_PERIOD;

    --State: set_reg
    BTNU <= '1';
    wait for CLK_PERIOD;

    --State: incr_sat
    wait for CLK_PERIOD;

    --State: wait_release_set_reg
    BTNU <= '0';
    wait for CLK_PERIOD;

    --State: set_sat
    BTNU <= '1';
    wait for CLK_PERIOD;

    --State: incr_sat
    wait for CLK_PERIOD;

    --State: wait_release_set_reg
    BTNU <= '0';
    wait for CLK_PERIOD;

    --State: set_reg
    BTNR <= '1';
    wait for CLK_PERIOD;

    --State: wait_release_confirm
    wait for 3*CLK_PERIOD;
    BTNR <= '0';
    wait for 10*CLK_PERIOD;
    timer_dn <= '1';
    wait;

    --State: wait_release_set_reg
    --State: set_reg

  end process test_bench;

end stimulus;
