----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/22/2021 05:51:54 PM
-- Design Name: 
-- Module Name: config_reg - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity config_reg is
  port (
    reg_select : in std_logic_vector (2 downto 0);
    reg_wr : in std_logic;
    reg : in std_logic_vector (9 downto 0);

    reg_hue : out std_logic_vector (9 downto 0);
    reg_sat : out std_logic_vector (9 downto 0);
    reg_val : out std_logic_vector (9 downto 0);

    rst : in std_logic;
    clk : in std_logic
  );
end config_reg;

architecture rtl of config_reg is
  -- Sélection du registre à écrire
  alias reg_select_hue is reg_select(2);
  alias reg_select_sat is reg_select(1);
  alias reg_select_val is reg_select(0);
begin

write_selected_register: process (clk, rst, reg_wr) is
begin
  if rst = '0' then
    if (rising_edge(clk) and reg_wr = '1') then
      if reg_select_hue = '1' then
        reg_hue <= reg;
      end if;

      if reg_select_sat = '1' then
        reg_sat <= reg;
      end if;

      if reg_select_val = '1' then
        reg_val <= reg;
      end if;
    end if;
  else
    reg_hue <= (others => '0');
    reg_sat <= (others => '0');
    reg_val <= (others => '0');
  end if;
end process write_selected_register;

end rtl;
