----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/10/2021 08:05:55 PM
-- Design Name: 
-- Module Name: top_level - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity top_level is
  port (
    enable        : out std_logic_vector(3 downto 0); -- écran gauche: enable(0)
    seven_segment : out std_logic_vector(7 downto 0); -- Ordre: 7 downto 0 => EDC<DP>BAFG
    sw_in         : in std_logic_vector(3 downto 0);  -- Ordre: 3 downto 0 => Up Down Right Left
    RESET         : in std_logic;
    CLK1          : in std_logic -- 100 MHz quartz
  );
end top_level;

architecture rtl of top_level is
  signal print_data_7seg : std_logic_vector(31 downto 0);
  alias char_1_7seg is print_data_7seg(31 downto 24);
  alias char_2_7seg is print_data_7seg(23 downto 16);
  alias char_3_7seg is print_data_7seg(15 downto 8);
  alias char_4_7seg is print_data_7seg(7 downto 0);
  
  -- _n signals accomodate negative logic peripheral wirings
  signal seven_segment_n : std_logic_vector(7 downto 0);
  alias a is seven_segment_n(2);
  alias b is seven_segment_n(3);
  alias c is seven_segment_n(5);
  alias d is seven_segment_n(6);
  alias e is seven_segment_n(7);
  alias f is seven_segment_n(1);
  alias g is seven_segment_n(0);
  alias dp is seven_segment_n(4);

  signal enable_n : std_logic_vector(3 downto 0);

  signal sw_in_debounced : std_logic_vector(3 downto 0);
  signal sw_in_n : std_logic_vector(3 downto 0);
  
  signal valid_clk : std_logic;
  signal clk_mmcm : std_logic;
  signal clk : std_logic;

  component clk_wiz_0
  port (
    clk_in : in std_logic;
    clk_out : out std_logic;
    reset : in std_logic;
    locked : out std_logic
  );
  end component;
begin

  -- Accomodates the inverted logic of 7-segments and their enable lines
  seven_segment <= not seven_segment_n;
  enable <= not enable_n;
  sw_in_n <= not sw_in_debounced;

  sys_clk : clk_wiz_0
    port map (
      clk_in  => CLK1,
      clk_out => clk_mmcm,
      reset => RESET,
      locked => valid_clk
    );

  BUFG_inst : BUFG
    port map (
       O => clk,
       I => clk_mmcm
    );

  menu_inst : entity work.menu(rtl)
    port map (
      ds_en => enable_n,
      seven_segment(7) => a,
      seven_segment(6) => b,
      seven_segment(5) => c,
      seven_segment(4) => d,
      seven_segment(3) => e,
      seven_segment(2) => f,
      seven_segment(1) => g,
      seven_segment(0) => dp,
      buttons => sw_in_n,

      reg_hue => open,
      reg_sat => open,
      reg_val => open,
    
      en => valid_clk,
      clk => clk
    );

  debouncer_inst : entity work.grp_debouncer(rtl)
    generic map (   
        N => 4, -- input bus width
        CNT_VAL => 10000 -- clock counts for debounce period
    )
    port map (
        clk_i => clk, -- system clock
        data_i => sw_in, -- noisy input data
        data_o => sw_in_debounced, -- registered stable output data
        strb_o => open -- strobe for new data available
    ); 
  
end rtl;
