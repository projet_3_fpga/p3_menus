----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/16/2021 01:12:51 PM
-- Design Name: 
-- Module Name: signed_to_str - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity signed_to_str is
    port ( signed_num : in std_logic_vector (9 downto 0);
           string_addr : out std_logic_vector (9 downto 0);
           clk : in std_logic;
           en : in std_logic
           );
end signed_to_str;

architecture rtl of signed_to_str is
  -- First number at addr 5
  constant OFFSET : natural := 395;
begin

  conversion: process(clk) is
  variable temp : signed (9 downto 0);
  begin
    if en = '1' and rising_edge(clk) then
      temp := signed(signed_num) + to_signed(OFFSET, signed_num'length);
      string_addr <= std_logic_vector(unsigned(temp));
    end if;
  end process conversion;

end rtl;
