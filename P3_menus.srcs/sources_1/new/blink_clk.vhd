----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/10/2021 08:04:59 PM
-- Design Name: 
-- Module Name: blink_clk - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments: 
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity blink_clk is
  port (
    clk_in_100MHz : in std_logic;
    en : in std_logic;
    
    -- Warning: Approximative timings, use for blinking only
    clk_out_2500ms : out std_logic; -- toggle every 2.5s (2.5s on, then 2.5s off)
    clk_out_1200ms : out std_logic;
    clk_out_600ms : out std_logic;
    clk_out_300ms : out std_logic;
    clk_out_150ms : out std_logic 
  );
end blink_clk;

architecture rtl of blink_clk is
  signal count : unsigned(28 downto 0);
  alias count_2_68s is count(28);
  alias count_1_34s is count(27);
  alias count_671ms is count(26);
  alias count_336ms is count(25);
  alias count_168ms is count(24);
begin

  counter : process(clk_in_100MHz, en) is
  begin
    if en = '0' then
      count <= (others => '0');
    elsif rising_edge(clk_in_100MHz) then
      count <= count + 1;
    end if;
  end process counter;
  
  clk_out_2500ms <= '0' when (count_2_68s = '0') else '1';
  clk_out_1200ms <= '0' when (count_1_34s = '0') else '1';
  clk_out_600ms  <= '0' when (count_671ms = '0') else '1';
  clk_out_300ms  <= '0' when (count_336ms = '0') else '1';
  clk_out_150ms  <= '0' when (count_168ms = '0') else '1';
  
  -- TODO: Registrer
end rtl;
