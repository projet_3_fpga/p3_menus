#!/usr/bin/python3
# Générer une LUT associant une chaîne à sa représentation 7-segments
# Date: 16-juin-2021

import os
import math

char_to_segments = {
    '0': "11111100",
    '1': "01100000",
    '2': "11011010",
    '3': "11110010",
    '4': "01100110",
    '5': "10110110",
    '6': "10111110",
    '7': "11100000",
    '8': "11111110",
    '9': "11110110",
    '.': "00000001",
    '-': "00000010",
    'A': "11101110",
    'a': "11111010",
    'b': "00111110",
    'C': "10011100",
    'c': "00011010",
    'd': "01111010",
    'E': "10011110",
    'F': "10001110",
    'G': "10111100",
    'H': "01101110",
    'h': "00101110",
    'I': "00001100",
    'J': "01111000",
    'L': "00011100",
    'n': "00101010",
    'O': "11111100",
    'o': "00111010",
    'P': "11001110",
    'q': "11100110",
    'r': "00001010",
    'S': "10110110",
    't': "00011110",
    'U': "01111100",
    'u': "00111000",
    'y': "01110110",
    ' ': "00000000",
    '\n': ""
}

def str_to_7seg(text):
    print_segments = ""
    for char in text:
        print_segments += char_to_segments[char];
    return print_segments

stringListFileName = "strings7seg.txt"
stringListFile = open(stringListFileName, 'r')

tab = "  "

segments = []
lines = []
linecount = 0
for line in stringListFile:
    segments.append(str_to_7seg(line))
    lines.append(line)
    linecount += 1;

closest_power_of_2 = pow(2, linecount.bit_length())
top_address = closest_power_of_2 - 1
current_address = top_address

# Affichage du VHDL
print("type rom_type is array (" + str(top_address) + " downto 0) of std_logic_vector(31 downto 0);")
print("signal ROM : rom_type := (")

for i in range(0, linecount):
    print(tab + '"' + segments[i] + '",' + "    -- " + hex(current_address).ljust(10) + " \"" + lines[i][:-1] + "\"")
    current_address -= 1

for i in range(linecount, top_address):
    print(tab + '"' + 32*'0' + '",' + "    -- " + hex(current_address).ljust(10) + " UNUSED")
    current_address -= 1
print(tab + '"' + 32*'0' + '"' + "     -- " + hex(current_address).ljust(10) + " UNUSED")

print(");")

exit(0)

